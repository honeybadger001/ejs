/*  This short program illustrates a phenomenon known as closure.
    Multiple instances of the variable can be alive at the same 
    time, another good illustration of the concept that local
    variables really are re-created for every call - different 
    calls cannot trample on one another's local variables.
    
    This feature - being able to reference a specifec instance
    of local variables in an enclosing function - is called 
    closure. A function that "closes over" some local variables
    is called a closure. This behaviour not only frees you 
    from having to worry about lifetimes of variables but 
    also allows for creative use of functions. 
    
    The explicit localvariable in wrapValue isn't necessary 
    since a parameter is itself a local variable.
*/


function wrapValue(n) {
	var localVariable = n;
	return function() { return localVariable }
}

var wrap1 = wrapValue(1);
var wrap2 = wrapValue(2);
console.log(wrap1()); // -> 1
console.log(wrap2()); // -> 2

/*  With a slight change, we can turn the previous example
    into a way to create functions that multiply by an 
    arbitrary amount.
*/
function multiplier(factor) {
	return function(number) {
		return number * factor++;
	}
}

var twice = multiplier(2);
console.log(twice(5)); // -> 10
console.log(twice(5)); // -> 15. No longer "twice 5"
console.log(twice(5)); // -> 20


/*  A good mental model is to think of the function keyword 
    "freezing" the code in its body and wrapping it into a 
    package (the function value). So when you read 
    return function(...){...},
    think of it as returning a handle to a piece of
    computation, frozen for later use.
    
    In the above example, multiplier returns a frozen chunk
    of code that gets stored in the twice variable. The last
    line then calls the valuein this variable, causing the 
    frozen code (return number*factor;) to be activated. It
    still has access to the factor variable from the mulitplier
    call that created it, and in addition it gets access to
    the argument passed when unfreezing it, 5, through
    its own number parameter. 
*/
