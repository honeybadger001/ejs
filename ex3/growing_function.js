/*  Design choices make functions easier to update 
    at a later time. Most changes cannot be anticipated
    but some coding practices are better than others
    (or at least more equal than others).
*/

function print_zero_padded_with_label(number, label) {
  var fwidth = 3;
  var number_string = String(number);
  while (number_string.length < fwidth)
    number_string = "0" + number_string;
  console.log(number_string + " " + label);
}


function print_farm_inventory(cows, chickens, pigs) {
  console.log("Farm inventory: ");
  print_zero_padded_with_label(cows, "cows");
  print_zero_padded_with_label(chickens, "chickens");
  print_zero_padded_with_label(pigs, "pigs");
  
}

// Alternatively,
function zero_pad(number, width) {
  var string = String(number);
  while(string.length < width)
    string = "0" + string;
  return string;
}

function print_farm_inventory_alt(cows, chickens, pigs) {
  console.log("Farm inventory:");
  console.log(zero_pad(cows,3), " cows");
  console.log(zero_pad(chickens,3), " chickens");
  console.log(zero_pad(pigs,3), " pigs");
}

print_farm_inventory(12, 15, 11);
print_farm_inventory_alt(12, 15, 11);


/*  One useful principle to keep in mind is not to add 
    cleverness unless you're absolutely sure you're going
    to need it. It can be tempting to write general 
    "frameworks" for every little bit of funtionality 
    you come across. Resist that urge. You won't get any
    real work done and you'll end up writing a lot of code
    that no one will ever use.
*/

/*  Side Effects

    Note that the function print_zero_padded_with_label 
    is used to produce a side effect (in this case printing
    to the console); the function zero_pad produces no side
    effect, it is used for its return value. 
    
    It is often more useful to encapsulate a procedure in 
    a function that has a return value but no side effect; 
    these sorts of functions are easier to combine with others.
    
    A pure function is a specific kind of value-producing 
    funciton that not only has no side effects but also
    doesn't rely on side effects from other code (e.g., does
    not read global variables which can be changed by other 
    code). A pure funtion has the pleasant property that,
    when called with the same arguments, it always produces 
    the same value (and does nothing else). This makes it 
    easy to reason about; a call to such a funtion can 
    be mentally substituted by its result without changing 
    the meaning of the code. When you are not sure that a 
    pure function is working correctly, you can test it by 
    simply calling it, and know that if it works in that 
    context, it will work in any context. Non-pure functions 
    may return different values based on a number of factors
    and might have side effects that might be hard to test 
    and think about. 
    
    That being said, side effects are often usedul and/or
    necessary. Some operations are easier to express in an
    efficent way when using side effects.
    
*/
