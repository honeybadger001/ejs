function even_odd(x) {
  if(x < 0)
    return even_odd(-x);
    
	if( x == 0 )
	  return "even";
	else if( x == 1 )
	  return "odd";
	else
	  return even_odd( x - 2 );
	  
}

var x = Number(prompt("Pick a number", ""));
if(!isNaN(x)){
	console.log(even_odd(x));
}else
	alert("That was not a number!");
