var count = 0;
function findSolution(target) {
  function find(start, history) {
    if (start == target)
      return history;
    else if (start > target){
      count++;
      return null;
    }else
      return find(start + 5, "(" + history + " + 5)")
      	  || find(start * 3, "(" + history + " * 3)");
  }
	
	return find(1, "1");
}

var x = Number(prompt("Pick a number", ""));
if(!isNaN(x)){
	var soln = findSolution(x);
	console.log("Tried and discarded ", count, " different paths.");
	if( !soln )
		console.log("Your number, ", x, ", cannot be reached from 1 by a sequence of operations of multiplying by 3 and/or adding 5");
	else
		console.log(x, " = ", soln);
}else
	alert("That was not a number!");
