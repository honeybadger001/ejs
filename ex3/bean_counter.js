function count_Bs(string) {
  return count_char(string, "B");
}

function count_char(string, c) {
 var N = string.length;
  var m = 0; // number of occurences of c
  for( var i = 0; i < N ; i++ )
    if(string.charAt(i) === c)
      m++;
  return m;

}

console.log(count_Bs("Banana-B"));
