var power = function( base, exponent){
	if(base == 0)
		return 0;
	var result = 1;
	for( var e = 0; e < exponent; e++)
		result *= base;
	return result;
};

console.log(power(2,10));
