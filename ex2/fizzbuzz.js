// print numbers from 1 to 100 except multiples of 3, 5;
var what;

for(var x = 1; x < 100; x++){
	what = "";
	if(x % 3 === 0 || x % 5 === 0){
		what += (x % 3) ? "" : "fizz";
		what += (x % 5) ? "" : "buzz";
		console.log(what);
		continue;
	}
	console.log(x);
}
