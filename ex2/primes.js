// print primes from 1 to 100;
var prime;
for(var x = 2; x <100; x++){
	prime = true;
	for(var i = 2; i <= Math.sqrt(x); i++){
		if( x % i == 0){
			prime = false;
			break;
		}
	}
	if(prime)
		console.log(x, " is prime.");
}
