function p(thing) {
  console.log(thing);
}

var A = [1,2,3,4,5];
p(A);
A.push(6);
p(A);

A.unshift(0);
p(A);

console.log(A.shift(0));
p(A);

p([1,2,3,4,3,1].indexOf(3));
p([1,2,3,4,3,1].lastIndexOf(3));

p([0,1,2,3,4].slice(2,4));
p([0,1,2,3,4].slice(2));

p("beans".slice(1));

function remove(array, index) {
  if( 0 <= index && index < array.length )
    return array.slice(0, index).concat(array.slice(index+1));
  
  return array;
}

p(remove([0,1,2,3,4,5,6], 3));
p(remove([0,1,2,3,4,5,6], 0));
p(remove([0,1,2,3,4,5,6], -1));
p(remove([0,1,2,3,4,5,6], 7));
p(remove([0,1,2,3,4,5,6], 8));

p("one two three".indexOf("ee"));
