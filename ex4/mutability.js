/*  numbers, strings and Booleans are all immutable - it is 
    impossible to change an existing value of those types.
    You can combine them and dervive new values from them,
    but when you take a specific string value, that value 
    will always remain the same. The text inside it cannot 
    be changed.
    
    With objects, on the other hamd, the content of a value can
    be modified by changing its properties. 
    
    When we have two numbers, 120 and 120, we can consider them 
    precisely the same number, whether or not they refer to 
    the same physical bits. But with objects, there is a 
    difference between having two references to the same object
    and having two different objects that contain the same 
    properties. 
*/

var object1 = { value: 10 };
var object2 = object1;
var object3 = { value: 10 };

console.log( object1 == object2 );
console.log( object1 == object3 );

/*  Javascripts == operator, when comparing objects, returns
    ture only if both objects are precisely the same value 
    (i.e., they refer to the same physical bits).
    Comparing different objects will return false, even
    if they have identical contents.
    
    There is no "deep" comparison operaion built into 
    JavaScript, which looks at an objects contents, 
    but it is possible to write one yourself
*/

