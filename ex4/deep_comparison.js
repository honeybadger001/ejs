function deep_equal(obj1, obj2){ 
  console.log("de: comparing", obj1, ", " , obj2);
  
  if( typeof(obj1) !== "object" || typeof(obj2) !== "object")
    return obj1 === obj2;
  
  /*
  if( (typeof(obj1) !== typeof(obj2)) 
    || obj1 == null || obj2 == null )
    return false;
  */

  //if( obj1 == obj2 ) return true;
  
  // Check whether they have identical properties
  for( var v in obj1 )
    if( !(v in obj2)) return false;

  for( var v in obj2 )
    if( !(v in obj1)) return false;
  
  // otherwise, the objects have the 
  // same properties and we can call deep_equal 
  // recursively
  for( var v in obj1 ){
    if( !deep_equal(obj1[v], obj2[v] )) return false;
  }
  return true;
}


console.log(1==1);
console.log(deep_equal(1,1));
A = [1,2,3];
console.log(A);
console.log([1,2,3] == [1,2,3]);
console.log("deep_equal: ", deep_equal([1,2,3],[1,2,3]));

var v1 = {nom: "albert", w: 180};
var v2 = {w: 180, nom: "albert"};
console.log("type: ", typeof(v1));
console.log("v1 == v2? ", v1 == v2);
console.log("deep_equal: ", deep_equal(v1, v2));

var v3, v4;
console.log("Test on undefined variables,");
console.log("deep_equal: ", deep_equal(v3, v4));
console.log("test on null, null");
console.log("deep_equal: ", deep_equal(null,null));

