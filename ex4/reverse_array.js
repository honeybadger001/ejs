function range (start, end, step) {
  if (step == undefined) step = 1;
  var array = [];
  if (step > 0)
  	for(var i = start; i <= end; i += step)
    	array.push(i);
  else if (step < 0)
    for(var i = start; i >= end; i += step)
    	array.push(i);
  return array;
}

function reverse_array(array) { 
  var newarr = [];
  var n = array.length;
  for (var i = 0; i < n; i++)
    newarr[i] = array[n-i-1];
  return newarr;
}

function reverse_array_in_place(array) {
  var temp;
  var n = array.length;
  for(var i = 0; i < n/2; i++){
    temp = array[i];
    array[i] = array[n-i-1];
    array[n-i-1] = temp;
  }

}

// =============================================

var A = range(0,7);
var B = range(0,8);
console.log("A:    ", A);
reverse_array_in_place(A);
console.log("A:rev ", A);

console.log("B:    ", B);
reverse_array_in_place(B);
console.log("B:rev ",B);

