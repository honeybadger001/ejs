function range (start, end, step) {
  if (step == undefined) step = 1;
  var array = [];
  if (step > 0)
  	for(var i = start; i <= end; i += step)
    	array.push(i);
  else if (step < 0)
    for(var i = start; i >= end; i += step)
    	array.push(i);
  return array;
}


function sum (array, step) {
  if (step == undefined)
    step = 1;
  var sum = 0;
  for(var i = 0; i < array.length; i += step)
    sum += array[i];
  return sum;
}

var A = range(1,10, 3);
var B = range(5,2, -1);
var C = range(1, 2, 0);
console.log(A);
console.log(sum(A));

console.log(B);
console.log(C);
