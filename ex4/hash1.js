var day1 = {
  squirrel : false,
  events : ["work", "touched tree", "pizza", "running",
            "television"]
};
  
var day2_events = ["work", "touched tree", "sushi", "weights",
            "television"];
            
var day2 = {
  squirrel : false,
  events : day2_events
};
            
console.log(day1.squirrel);
console.log(day1.wolf);
day1.wolf = false;
console.log(day1.wolf);

console.log(day2.events);
day2_events[0] = "school";
console.log(day2.events);
day2.events[4] = "reading";
console.log(day2_events);

// properties whole names are not valid variable names 
// or numbers have to be quoted. e.g.,
var descriptions = {
	work : "Went to work",
	"touched tree" : "touched a tree"
};

delete day2.events;
console.log(day2.events);
console.log("events" in day2);
console.log("squirrel" in day2);
console.log(day2_events);
delete day2_events[3];
console.log(day2_events);
console.log(day2_events[3]);
console.log(day2_events[4]);
delete day2_events;
console.log(day2_events);
