/*  Whenever a function is called, a special variable named arguments
    is added to the environment in which the function body runs. This
    variable refers to an object that holds all of the arguments
    passed to the function. Remember that in JavaScript you are
    allowed to pass more or fewer arguments to the 
    function than the number of parameters the function itself
    declares.
    
*/

function argument_counter() {
  console.log("You gave me " + arguments.length + " arguments.");
}

argument_counter("Straw man", "Tautology", "Ad hominem");

// Recall interface to function that added entries to 
// Jacques' journal,
// addEntry(["work", "touched tree", "pizza"], false);
// can create an alternative that is easier to call
/*

function addEntry(squirrel) {
  var entry = {events: [], squirrel: squirrel};
  for (var i = 1; i < arguments.length; i++)
    entry.events.push(arguments[i]);
  journal.push(entry);
}

addEntry(true, "work", "touched tree", "pizza");

*/
