function array_to_list(array){
  if( array.length > 0 ){
    var element = { value: array[0],
      next : array_to_list(array.slice(1)) };
    return element
  }else 
    return null;
}


function list_to_array(list) {
  var array = [];

  for( var i = 0; list != null; i++, list = list.next)
    array[i] = list.value;
  
  return array;
}


function append( list, item ) {
  if( list != null && item != null ){
    while(list.next != null) 
      list = list.next
    list.next = { value: item, next: null };
  }
}


function print_list(list) {
  while (list != null) {
    console.log(list.value);
    list = list.next;
  }
}



var A = [0,1,2,3,4,5,6,7,8];
var L = array_to_list(A);

var words = ["these", "words", "make", "a", "sentence"];
var word_list = array_to_list(words);
append(word_list, "!");
print_list(word_list);

console.log(list_to_array(word_list));

