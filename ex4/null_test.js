console.log("null === null: ", null === null);
console.log("null === undefined: ", null === undefined);
console.log("undefined === null: ", undefined === null);
console.log("undefined === undefined: ", undefined === undefined);

console.log("null == null: ", null == null);
console.log("null == undefined: ", null == undefined);
console.log("undefined == null: ", undefined == null);
console.log("undefined == undefined: ", undefined == undefined);


console.log("typeof(null): ", typeof(null));
console.log("typeof(undefined): ", typeof(undefined));
