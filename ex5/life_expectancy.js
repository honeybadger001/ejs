/*  Compute life expectancy by century.
    Assign centuries by year of death  */

var ancestry = JSON.parse(ANCESTRY_FILE);
console.log(ancestry[0].mother);

function age_at_death(person) {
  return person.died - person.born;
}

function death_range(ancestry) {
  var earliest = 5000;
  var latest = 0;
  var current = 0;
  ancestry.forEach( function(person) {
    if ((current=person.died) != undefined && current < earliest)
      earliest = current;
    if (current && current > latest)
      latest = current;
  });
  return [earliest, latest];
}

var range = death_range(ancestry);
console.log("earliest death:", range[0]);
console.log("latest death:", range[1]);

nbins = Math.ceil(range[1]/100) - Math.floor(range[0]/100);
console.log(nbins);
earliest = Math.floor(range[0]/100);

age_data = [0,0,0,0,0,0];
counts = [0,0,0,0,0,0];

ancestry.forEach( function (person) {
    var bin = Math.floor(person.died/100) - earliest;
    age_data[bin] += age_at_death(person);
    counts[bin]++;
  }
);

//console.log("average age at death by century");
console.log(counts);
/*
var avg;
for (var i = 0; i < counts.length; i++){
  if (counts[i] != 0 ){
    avg = age_data[i]/counts[i];
    console.log(earliest + i + 1, ": ", avg.toFixed(2));
  }else{
    console.log(earliest + i + 1, ": no data");
  }
}
*/
// abstraction of the grouping operation
function group_by(array, group) {
  var groupings = {};
  array.forEach( function(item) {
    var group_name = group(item);
    if (group_name in groupings)
      groupings[group_name].push(item);
    else
      groupings[group_name] = [item];
  });
  return groupings;
}

function century_of_death(person){
  return Math.ceil(person.died/100);
}

var bins = group_by(ancestry, century_of_death);

console.log(bins);
