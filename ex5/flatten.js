A = [1,2,3,4];
B = [5,6,7,8];
C = [9,10,11,12];

abc = [A,B,C];

function reduce(array, combine, start) {
  var current = start;
  for (var i = 0; i < array.length; i++)
    current = combine(current, array[i]);
  return current;
}


flat = abc.reduce(function(a,b) {
    return a.concat(b);
  }
);

flat2 = reduce(abc, function(a,b) {
    return a.concat(b);
  }, []
);

//flat3 = abc.reduce( concat.bind(a,b));

console.log(flat2);
