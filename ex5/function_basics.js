// function as argument
function forEach(array, action) {
  for( var i = 0; i < array.length; i++ )
    action(array[i]);
}

//forEach(["Wampeter", "Foma", "Granfalloon"], alert);

var numbers = [1,2,3,4,5,6], sum = 0;

forEach(numbers, function(number){
  sum += number; 
  });
console.log(sum);

/*

function gatherCorrelations(journal) {
  var phis = {};
  forEach(journal, function(entry){
    forEach(entry.events, function(givenEvent){
      if(!(givenEvent in phis))
        phis[givenEvent] = phi(tableFor(givenEvent,journal));
      })
    })
  return phis;
}

function gatherCorrelations(journal) {
  var phis = {};
  journal.forEach(function(entry) {
    entry.events.forEach(function(givenEvent){
      if (!(givenEvent in phis))
        phis[givenEvent] = phi(tableFor(givenEvent,journal));
      })
    })
  return phis;
}

*/


function greaterThan(n) {
  return function(m) { return m > n; };
}

var greaterThan10 = greaterThan(10);
console.log("11 > 10 ? " , greaterThan10(11));

// functions that change other functions

function noisy(f) {
  return function(arg) {
    console.log("calling with ", arg);
    var val = f(arg);
    console.log("called with ", arg, " - got ", val);
    return val;
  };
}

noisy(Boolean)(true);
    
function unless(test, then) {
  if(!test) then();
}
function repeat(times, body) {
  for (var i = 0; i < times; i++) body(i);
}

repeat(3, function(n) {
  unless(n % 2, function() {
    console.log(n, "is even");
  });
});

function transparentWrapping(f) {
  return function() {
    return f.apply(null, arguments);
  };
}

var myfunc = transparentWrapping(Math.min);
console.log(myfunc(2,5));
