function every(array, predicate) {
  array.forEach( function (item) {
      if (predicate(item) === false)
        return false;
    }
  );
  return true;
}

function some(array, predicate) {
  array.forEach( function (item) {
      if (predicate(item) === true)
        return true;
    }
  );
  return false;
}
