//requires ancestry.js

var ancestry = JSON.parse(ANCESTRY_FILE);
console.log(ancestry[0].mother);

var byName = {};
ancestry.forEach( function(person) { 
    byName[person.name] = person;
  }
);


function motherAge(person) {
  var born = person.born;
  if(byName[person.mother] != null)
    var mborn = byName[person.mother].born;
  else
    return -1;  // return a special value so we know not to include it
  
  return born - mborn;
}

console.log(motherAge(byName["Philibert Haverbeke"]));

function reduceAncestors(person, f, defaultValue) {
  function valueFor(person) {
    if (person == null)
      return defaultValue;
    else
      return f(person, valueFor(byName[person.mother]),
                       valueFor(byName[person.father]));
  }
  return valueFor(person);
}

function averageMotherAge( person, fromMother, fromFather ) {
  /*
  if (fromMother > 0 && fromFather > 0)
    return (motherAge(person) + fromMother + fromFather) / 3;
  else if (fromMother > 0 && fromFather <= 0)
    return (motherAge(person) + fromMother) / 2;
  else if (fromMother <= 0 && fromFather > 0)
    return (motherAge(person) + fromFather) / 2;
  else
    return motherAge(person);
  */
  return motherAge(person) + fromMother + fromFather;
}

var ph = byName["Philibert Haverbeke"];
//console.log( reduceAncestors(ph, averageMotherAge, -1) );

var count = 0;
var total = 0;
var age;
ancestry.forEach( function(person) {
  if ((age=motherAge(person)) > 0){
    total += age;
    count++;
  } // else do nothing
})
if(count > 0)
  console.log("Average age of mothers at birth: ", (total/count).toFixed(2));
  
