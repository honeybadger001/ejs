//requires ancestry.js

/*  JSON stands for JavaScript Object Notation. It is widely used
    as a data storage and communication format on the Web.
    
    JSON is similar to JavaScript's wat of writing arrays 
    and objects, with a few restrictions. All property names
    have to be surrounded by double quotes, and only simple 
    expressions are allowed - no function calls, varaibles, 
    or anything else that involves actual computation. 
    COmments are not allowed in JSON.
    
*/
/*
var string = JSON.stringify({name: "X", born: 1980});
console.log(string);
console.log(JSON.parse(string).born);
*/

var ancestry = JSON.parse(ANCESTRY_FILE);
console.log(ancestry.length);

function filter(array, test) {
  var passed = [];
  for (var i = 0; i < array.length; i++) {
    if (test(array[i])) 
      passed.push(array[i]);
  }
  return passed;
}

console.log(filter(ancestry, function(person) {
  return person.born > 1900 && person.born < 1925;
})[0]);

console.log(ancestry.filter(function(person) {
  return person.father == "Carel Haverbeke"
})[0]);


// the following map method transforms an array by applying
// a function to all of its elements and building a new
// array from the returned values.

function mapp(array, transform) { 
  var mapped = [];
  for (var i = 0; i < array.length; i++)
    mapped.push(transform(array[i]));
  return mapped;
}

var overNinety = ancestry.filter(function(person) {
  return person.died - person.born > 90;
});

console.log(mapp(overNinety, function(person) {
  return person.name;
}));


// The reduce function returns a particular value 
// which attempts to describe the contents of an 
// array in one way or another. 
// e.g., mean, median, mode, max, min, sum, product, etc
function reduce(array, combine, start) {
  var current = start;
  for (var i = 0; i < array.length; i++)
    current = combine(current, array[i]);
  return current;
}

console.log( reduce([1,2,3,4], function(a,b) {
    return a + b;
  }, 0)
);
  
// most ancient known ancestor
console.log( ancestry.reduce( function(oldest, person){
    if( person.born < oldest.born ) return person;
    else return oldest;
  })
);

/*  The above examples can be rewritten without higher 
    order functions without adding much complexity to the
    code. 
    
    Higher order functions really start to shine when you 
    need to compose functions.
*/

// Let's find the average age for men and women
// in the data set
function average(array) {
  function plus(a, b) { return a + b; }
  return array.reduce(plus) / array.length;
}
function age(p) {return p.died-p.born;}
function male(p) {return p.sex == "m";}
function female(p) {return p.sex == "f";}

console.log(average(ancestry.filter(male).map(age)).toFixed(2));
console.log(average(ancestry.filter(female).map(age)).toFixed(2));

// tracing lineage
var byName = {};
ancestry.forEach(function(person) {
  byName[person.name] = person;
});
var fatherName = "Philibert Haverbeke";
console.log(byName[fatherName]); // this works

// is A an ancestor of B?
function isAncestor(byName, ancestorName, personName) {
  var current;
  if (ancestorName == personName) return true;
  
  if ( (current=byName[personName]) == undefined ){
    //console.log("No record of \"", personName, "\".");
    return false;
  }
  
  if( (current.mother != null)
        || (current.father != null)){
    return ( isAncestor(byName, ancestorName, current.mother)
        || isAncestor(byName, ancestorName, current.father))}
  else return false;
}

// Another possibility, from textbook:
function reduceAncestors(person, f, defaultValue) {
  function valueFor(person) {
    if (person == null)
      return defaultValue;
    else
      return f(person, valueFor(byName[person.mother]),
                       valueFor(byName[person.father]));
  }
  return valueFor(person);
}

function sharedDNA(person, fromMother, fromFather) {
  if (person.name == "Pauwels van Haverbeke")
    return 1;
  else
    return (fromMother + fromFather) / 2;
}

var ph = byName["Philibert Haverbeke"];

//console.log( isAncestor(byName,"Pauwels van Haverbeke", "Philibert Haverbeke"));

console.log((reduceAncestors(ph, sharedDNA, 0) / 4).toPrecision(3));

function countAncestors(person, test) {
  function combine(current, fromMother, fromFather) {
    var thisOneCounts = current != person && test(current);
    return fromMother + fromFather + (thisOneCounts ? 1 : 0); 
  }
  return reduceAncestors(person, combine, 0);
}

console.log("How many lived to 70? ",
  countAncestors(ph, function (person) {
    return (person.died - person.born)>= 70; 
  })
);


// The bind method
// ===============

var theSet = ["Carel Haverbeke", "Maria van Brussel", "Donald Duck"];

function isInSet(set, person) {
  return set.indexOf(person.name) > -1;
}

console.log(ancestry.filter( function(person) {
    return isInSet(theSet, person);
  }
));

// alternatively... 
console.log(ancestry.filter(isInSet.bind(null, theSet)));

/*  The call to bind returns a function that will call
    isInSet with theSet as the first argument, followed 
    by any remaining arguments given to the bound
    function.
*/

