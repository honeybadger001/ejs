function Rabbit(type) {
  this.type = type;
}

var killer_rabbit = new Rabbit("killer");
var black_rabbit = new Rabbit("black");
console.log(black_rabbit.type);
console.log(Object.getPrototypeOf(black_rabbit) == Rabbit.prototype);
console.log(Object.getPrototypeOf(black_rabbit) == Object.getPrototypeOf(Rabbit)); 

/*  Constructors (in fact all functions) automatically get a property
    named prototype, which by default holds a plain, empty object 
    that derives from Object.prototype. Every instance created with 
    this constructor will have this object as its prototype. So to 
    add a speak method to rabbits created with the Constructor,
    we can simply do the following:
*/
Rabbit.prototype.speak = function (line) {
  console.log("The " + this.type + " rabbit says '" + line + "'");
};

black_rabbit.speak("Doom...");

/*  Important to distinguish between the way a prototype is associated 
    with a constructor (through its prototype property) and the way 
    objects \emph{have} a prototype (which can be retrieved with
    Object.getPrototypeOf). The actual prototype (field) of a constructor
    is Function.prototype since constructors are functions. Its 
    prototype \emph{property} will be the prototype of instances 
    created through it but not its \emph{own} prototype.
*/


Rabbit.prototype.hop = function () {
  console.log("The quick " + this.type + " rabbit hops over the lazy dog.");
};

black_rabbit.hop();



