/*  Objective: write a program that, given an array of table cells
    will build up and output a string that contains a nicely laid out
    table - that is, the columns are straight and the rows aligned.
*/

/*  The interface:
    minHeight() - returns a number indicating the minimum height this
    cell requires (in lines).
    
    minWidth() - returns a number indicating the cell's minimum width 
    (in characters).
    
    draw(width, height) returns an array of length height, which 
    contains a series of strings that are each width characters
    wide. This represents the contents of the cell.
*/

/*  First part of the program computes arrays of minimum column widths
    and row heights for a grid of cells. The rows variable will hold 
    an array of arrays, with each inner array representing a row of cells.
*/
function rowHeights(rows) {
  return rows.map( function(row) {
    return row.reduce( function(max, cell) {
      return Math.max(max, cell.minHeight());
    }, 0);
  });
}

function myColWidths(rows) { 
  var numCols = 0;
  numCols = rows.reduce(function(numCols,row) {
    return Math.max(numCols, row.length);
  }. 0);

  var colWidths = [];
  var cell;
  for(var j = 0; i < numCols; j++) {
    
    var width = 0;
    for (var i = 0; i < rows.length; i++) {
      if (cell = rows[i][j]) // assign and compute if non-null
        width = (cell.minWidth > width) ? cell.minWidth : width;
    }
  }
}

/*  Using a variable names starting with an underscore (_), or 
    consisting solely of a single underscore is a way to indicate 
    to human readrers that this argument will not be used.
    
    The map method (as well as forEach, filer, and similar array
    methods) passes a second argument to the callback function 
    it is given: the index of the current element. My mapping over
    the elements of the first row and using only the map function's 
    second argument, colWidths builds up an array with one element 
    for every column index. The call to reduce then runs over 
    the outer rows array and on each iteration for the given index
    picks out the width of the widest cell at that index.
*/
function colWidths(rows) {
  return rows[0].map( function(_, i) {
    //var width = 0;
    return rows.reduce(function(width, row) {
      return Math.max(width, row[i].minWidth());
    }, 0);
  };
}
