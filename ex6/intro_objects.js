var rabbit = {};

/*  Methods are simply property values that hold functions  */
// can define mathods on individual objects
rabbit.speak = function(line) {
  console.log("The rabbit says '" + line + "'");
};

rabbit.speak("I'm alive!");
/*  Usually a method needs to do something with the object it was
    called on. When a function is called as a method - looked up
    as a property and immediately called, as in object.method() - the
    special variable this in its body will point to the object that
    it was called on.
*/
function speak(line) {
  console.log("The " + this.type + " rabbit says '" + line + "'");
}

var white_rabbit = {type: "white", speak: speak};
var fat_rabbit = {type: "fat", speak: speak};
var black_rabbit = {type: "black"};

