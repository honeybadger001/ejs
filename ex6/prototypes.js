var empty = {};
console.log(empty.toString);

console.log(empty.toString());

/*  In addition to their set of properties, almost all objects
    also have a prototype. A prototype is another object that
    is used as a fallback source of properties. When an object 
    gets a request for a property that it does not have, its
    prototype will be searched for the property, then the 
    protoype's prototype, and so on.
    So who is the prototype of the empty object? It is the great 
    ancestral prototype, the entity behind almost all object,
    Object.prototype.
*/

console.log(Object.getPrototypeOf({}) == Object.prototype);
console.log(Object.getPrototypeOf(Object.prototype));
console.log(Object.getPrototypeOf(empty.toString) == Function.prototype);
console.log(Function.prototype);

/*  The prototype relations of JavaScript objects form a tree-shaped
    structure, and at the apex of this structure sits 
    Object.prototype.  It provides a few methods that show up in all
    objects, such as toString.
    
    Many objects dont directly have Object.prototype as their 
    prototype, but instead have another object, which provides its
    oen default properties. 
    
    Functions derive from Function.prototype, arrays derive from
    Array.prototype.
*/

console.log(Object.getPrototypeOf(isNaN) == Function.prototype);
console.log(Object.getPrototypeOf([]) == Array.prototype);

/*  Use Object.create to create an object with a specific prototype  */
var proto_rabbit = {
  speak: function(line) {
    console.log("The " + this.type + " rabbit says '" + line + "'");
  }
};

var killer_rabbit = Object.create(proto_rabbit);
killer_rabbit["type"] = "killer"; // or killer_rabbit.type = "killer";
killer_rabbit.speak("SKREEE!");
console.log(Object.getPrototypeOf(killer_rabbit));



