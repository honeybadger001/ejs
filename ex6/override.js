/*  When you add a property to an object, whether it is present in the
    prototype or not, the property is added to the object \emph{itself},
    which will henceforth have it a its own property. If there \emph{is} a
    property by the same name in the prototype, this original property will 
    no longer affect this object. The prototype itself is unchanged.
*/
function Rabbit(type) {
  this.type = type;
}

// the following is true
console.log(Object.getPrototypeOf(Rabbit.prototype) == Object.prototype);
var killer_rabbit = new Rabbit("killer");
var black_rabbit = new Rabbit("black");

Rabbit.prototype.speak = function (line) {
  console.log("The " + this.type + " rabbit says '" + line + "'");
};


Rabbit.prototype.teeth = "small";
console.log(killer_rabbit.teeth);
killer_rabbit.teeth = "long, sharp, and bloody";
console.log(killer_rabbit.teeth);
console.log(Rabbit.prototype.teeth);
console.log(black_rabbit.teeth);

black_rabbit.toString = function () {
  return "black rabbit";
  };
console.log(black_rabbit + " says haha");


console.log(Array.prototype.toString == Object.prototype.toString);
console.log(Rabbit.prototype.toString == Object.prototype.toString);

console.log([1,2] + " are two whole numbers");
console.log([1,2].toString());
console.log(Object.prototype.toString.call([1,2]));


Rabbit.prototype.dance = function() {
  console.log("The " + this.type + " rabbit dances a jig.");
};
black_rabbit.dance();

/* Prototype interference */

var map = {};
function store_phi(event, phi) {
  map[event] = phi;
}

store_phi("pizza", 0.069);
store_phi("touched tree", -0.081); 

Object.prototype.nonsense = "hi";
for(var name in map) 
  console.log(name);
console.log("nonsense" in map);
console.log("toString" in map);
delete Object.prototype.nonsense;


black_rabbit.dance();
delete black_rabbit.dance;//does nothing
//delete Rabbit.prototype.dance;  // rabbits cannot dance anymore
black_rabbit.dance(); //

/*  Note that toString did not show up in the for/in loop, but the 
    in operator did return true for it. JavaScript distinguishes
    between enumerable and nonenumerable properties.
    ALl properties that we create simply by assigning to them 
    are enumerable (eg, pizza, touched tree). The standard 
    properties in Object.prototype are nonenumerable, which 
    is why they don't appear in the for/in loop. 
    
    It is possible to define our own nonenumerable properties by using 
    the Object.defineProperty function, which allows us to control the 
    type of property we are creating.
    
*/
Object.defineProperty(Object.prototype, "hiddenNonsense", 
                      {enumerable: false, value: "hi"});
                      

for (var name in map)
  console.log(name);
console.log(map.hiddenNonsense);
console.log("object prototype? ", Object.getPrototypeOf(map) == Object.prototype);

/*  But the in operator still tells us that Object.prototype
    properties exist in our object. TO circumvent that we can
    use the object's hasOwnProperty method. This method tells 
    us whether the object \emph{itself} has the property, 
    without looking at its prototypes.
    
    useful outline for processing properties in an object:
    for (var name in map) { // incudes properties in prototype
      if (map.hasOwnProperty(name)) {
*/
console.log(map.hasOwnProperty(toString));
console.log(map.hasOwnProperty("pizza"));
console.log(map.hasOwnProperty("hiddenNonsense"));

/*  Prototype-Less objects
    Create prototype-less objects by passing null to the 
    Object.create funtion to create an object without 
    a specific prototype
 */ 

var map1 = Object.create(null);
map1.phi = function(event, phi) {
  this[event] = phi;
}

map1.phi("pizza", 0.069);
console.log("toString" in map1);
console.log("pizza" in map1);

/*  Polymorphism refers to the phenomenon that a piece of code 
    will affect different objects with the same interface differently
    because differing behavoiur can be programmed into the objects 
    and/or their prototypes. Polymorphic code can work with 
    different objects that present the appropriate interface.
*/

