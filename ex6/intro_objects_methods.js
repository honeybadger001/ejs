var rabbit = {};

/*  Methods are simply property values that hold functions  */
// can define mathods on individual objects
rabbit.speak = function(line) {
  console.log("The rabbit says '" + line + "'");
};

rabbit.speak("I'm alive!");
/*  Usually a method needs to do something with the object it was
    called on. When a function is called as a method - looked up
    as a property and immediately called, as in object.method() - the
    special variable this in its body will point to the object that
    it was called on.
*/
function speak(line) {
  console.log("The " + this.type + " rabbit says '" + line + "'");
}

var white_rabbit = {type: "white", speak: speak};
var fat_rabbit = {type: "fat", speak: speak};
var black_rabbit = {type: "black"};

white_rabbit.speak("Oh my ears and whiskers, " +
                   "how late it's getting!");
fat_rabbit.speak("I could sure use a carrot right now.");

speak("hello");

/*  The bind method associates to a function, in the following 
    case 'speak' an object with which it can reference 'this'.
    The function can then be assigned to the object itself
    (as below) and now black_rabbit has a speak method too.
*/
black_rabbit.speak = speak.bind(black_rabbit);
black_rabbit.speak("me too!");

/*  The apply and bind methods both take a first argument that can 
    be used to simulate method calls. The first argument is in fact
    used to give a value to this.
    
    There is a method similar to apply called call. It also calls 
    the function it is a method of but takes its arguments normally
    rather than as an array. Like apply and bind, call can be 
    passed a specific this value.
*/



speak.apply(black_rabbit, ["Burp!"]);
 //black_rabbit.speak("haha"); //**error** without the call to bind above

speak.call({type: "old"}, "Oh my.");
